package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.service.ITaskService;
import ru.t1.lazareva.tm.model.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService
public class TasksEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Nullable
    @WebMethod(operationName = "findAll")
    @GetMapping()
    public List<TaskDto> get() {
        return taskService.findAll();
    }

    @WebMethod(operationName = "saveAll")
    @PostMapping
    public void post(@WebParam(name = "tasks", partName = "tasks") @NotNull @RequestBody List<TaskDto> tasks) {
        taskService.saveAll(tasks);
    }

    @WebMethod(operationName = "updateAll")
    @PutMapping
    public void put(@WebParam(name = "tasks", partName = "tasks") @NotNull @RequestBody List<TaskDto> tasks) {
        taskService.saveAll(tasks);
    }

    @WebMethod(operationName = "deleteAll")
    @DeleteMapping()
    public void delete() {
        taskService.removeAll();
    }

}