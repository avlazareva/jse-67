package ru.t1.lazareva.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    public TaskBindToProjectRequest(@Nullable String token) {
        super(token);
    }

    public TaskBindToProjectRequest(@Nullable final String token, @Nullable final String taskId, @Nullable final String projectId) {
        super(token);
        this.taskId = taskId;
        this.projectId = projectId;
    }

}
