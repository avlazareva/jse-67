package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.service.IProjectService;
import ru.t1.lazareva.tm.model.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService
public class ProjectsEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @WebMethod(operationName = "findAll")
    @GetMapping()
    public List<ProjectDto> get() {
        return projectService.findAll();
    }

    @WebMethod(operationName = "saveAll")
    @PostMapping
    public void post(@WebParam(name = "projects", partName = "projects") @NotNull @RequestBody List<ProjectDto> projects) {
        projectService.saveAll(projects);
    }

    @WebMethod(operationName = "updateAll")
    @PutMapping
    public void put(@WebParam(name = "projects", partName = "projects") @NotNull @RequestBody List<ProjectDto> projects) {
        projectService.saveAll(projects);
    }

    @WebMethod(operationName = "deleteAll")
    @DeleteMapping()
    public void delete() {
        projectService.removeAll();
    }

}