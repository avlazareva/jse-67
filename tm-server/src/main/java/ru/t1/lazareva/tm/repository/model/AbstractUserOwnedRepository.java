package ru.t1.lazareva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> {

    @NotNull
    M addByUserId(
            @NotNull final String userId,
            @NotNull final M model
    );

    void clearByUserId(@NotNull final String userId);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    List<M> findAllByUserId(@NotNull final String userId);

    @Nullable
    List<M> findAllByUserId(@NotNull final String userId, @Nullable final Comparator comparator);

    @Nullable
    M findOneByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    M findOneByUserIdAndIndex(@NotNull final String userId, @NotNull final Integer index);

    int getSizeByUserId(@NotNull final String userId);

    void removeByUserId(@NotNull final String userId, @NotNull final M model);

    void removeByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    M updateByUserId(
            @NotNull String userId,
            @NotNull M model
    );
}
