package ru.t1.lazareva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.dto.request.TaskRemoveByIndexRequest;
import ru.t1.lazareva.tm.event.ConsoleEvent;
import ru.t1.lazareva.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-remove-by-index";

    @NotNull
    private static final String DESCRIPTION = "Remove task by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken());
        request.setIndex(index);
        taskEndpoint.removeTaskByIndex(request);
    }

}